package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;

import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true){
            System.out.println("Let's play round "+ roundCounter); // Declares new Round
            roundCounter += 1;
            String inputPlayer = "";
            while (true) {
                inputPlayer = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase(); // Takes input from player
                boolean check = checkInput(inputPlayer);
                if (check == true){
                    break;
                }
                else {System.out.println("I do not understand "+inputPlayer+". Could you try again?");}
            }
            String inputComp = randChoice();
            Boolean winner = true;
            if (inputComp.equals(inputPlayer)) {
                System.out.println("Human chose "+ inputComp +". computer chose "+ inputComp +". It's a tie!");
            }
            else{
                winner = isWinner(inputPlayer, inputComp);
                if ( winner == true){
                    humanScore += 1;
                    System.out.println("Human chose "+ inputPlayer +", computer chose "+ inputComp +". Human wins!");
                }
                else{
                    computerScore += 1;
                    System.out.println("Human chose "+ inputPlayer +", computer chose "+ inputComp +". Computer wins!");
                }
            }
            
            System.out.println("Score: human "+humanScore+ ", computer "+computerScore);
            if (!continuePlaying()){
                break;
            }

        }
        System.out.println("Bye bye :)");

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String randChoice(){
        Random random_choice = new Random();
        int intIndeks = random_choice.nextInt(rpsChoices.size()); // Generates random number 
        String output = rpsChoices.get(intIndeks); // Takes random number and gives corresponding string output
        return output;

    }


    public Boolean isWinner(String inputPlayer, String inputComp){
        if(inputComp.equals("rock") && inputPlayer.equals("paper")){
            return true;
        }
        else if(inputPlayer.equals("rock") && inputComp.equals("scissors") ){
            return true;
        }
        else if (inputPlayer.equals("scissors") && inputComp.equals("paper")){
            return true;
        }
        else{
            return false;
        }


    }

    public Boolean checkInput(String inputPlayer){
        if (rpsChoices.contains(inputPlayer)){
            return true;
        }
        else{
            return false;
        }
    }

    public Boolean continuePlaying(){
        while (true){
            String cont = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (cont.equals("y")){
                return true;
            }
            else if (cont.equals("n")){
                return false;
            }
            else {
                System.out.println("I don't understand "+cont+". Try again");
            }
        }
    }
}

